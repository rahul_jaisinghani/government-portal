<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<h2 style="text-align: center">Eligibility Criteria</h2>
<form class="form-horizontal" method="post" action="tables/elig.php">
    <br>
    <br>
    <div class="form-group">

        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Age:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="number" class="form-control" id="elig_age" placeholder="Enter age" name="elig_age" min="0">
            </div>
        </div>
    </div>



    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>State:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="elig_state" placeholder="Enter state name" name="elig_state">
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Occupation:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="elig_occupation" placeholder="Enter occupation" name="elig_occupation">
            </div>
        </div>
    </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-5">
                <button type="submit" class="btn btn-default" style="text-align: center">Submit</button>
            </div>
        </div>
    </div>
</form>


