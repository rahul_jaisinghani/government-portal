<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta charset="utf-8">
  <title>Government Portal</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="gov.jpg" rel="icon">
  <link href="gov" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->


  <!-- Libraries CSS Files -->

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto">Reve<span>al</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#body">Home</a></li>
          <li><a href="index.html">About Us</a></li>
          <li><a href="index.html">Services</a></li>
          <li><a href="index.html">Portfolio</a></li>
          <li><a href="index.html">Contact</a></li>
          <li><a href="LoginForm.php">Login</a></li>
          </li>

        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

<br>
<br>
<br>



<h2 style="text-align: center">SignUp Form</h2>
<form class="form-horizontal" method="post" action="SignIn.php">
    <br>
    <br>

    <div class="form-group">
            <div class="col-xs-12">
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <label>UserName:</label>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="username" placeholder="Enter user name" name="username">
                </div>
            </div>
        </div>



    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Email:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>
        </div>
    </div>



    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Password:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
            </div>
        </div>
    </div>


       <div class="col-xs-12">
        <div class="col-xs-4"></div>
        <div class="col-xs-5"><Strong>Select image to upload:</strong></div></div>
       <br>
       <br>
       <div class="col-xs-12">
               <div class="col-xs-4"></div>
               <div class="col-xs-3"> <input type="file" name="aadhar" id="aadhar">
                                      </div>
                                      <div class="col-xs-3"> <input type="file" name="pan" id="pan">
                                                                            </div>


                                      </div>




<br>
<br>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-5">
                <button type="submit" class="btn btn-default" style="text-align: center">Submit</button>
            </div>
        </div>
    </div>

</form>


