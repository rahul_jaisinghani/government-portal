<?php



require '../connection.php';
$conn    = Connect();

$query   = "SELECT * FROM scheme ";
$success = $conn->query($query) or die(mysql_error());
;

if (!$success) {
    die("Couldn't enter data: ".$conn->error);

}





?>
<!DOCTYPE html>
<html>
<head>
    <title>Table</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="css/tables.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div class="header">

</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-12">

            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">Schemes List</div>
                </div>
                <div class="panel-body" style="overflowauto;">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Details</th>
                            <th>Eligibility Age</th>
                            <th>Eligibility Occupation</th>
                                                        <th>Eligibility state</th>
                            <th>Required Documents</th>
                            <th>Type</th>
                            <th>Apply</th>

                        </tr>
                        </thead>

                        <tbody>
                        <?php  ?>
                        <?php while($row= mysqli_fetch_assoc($success)) { ?>
                            <tr>
                                <td><?php echo $row['name'];?></td>
                                <td><?php echo $row['details'];?></td>
                                <td><?php echo $row['elig_age'];?></td>
                                <td><?php echo $row['elig_occupation'];?></td>
                                <td><?php echo $row['elig_state'];?></td>
                                <td><?php echo $row['terms'];?></td>
                                <td><?php echo $row['type'];?></td>
                                <td><a href="<?php echo $row['url'];?>" target="_blank"><button type="button" class="btn btn-primary">Apply</button></a></td>






                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>



        </div>
    </div>
</div>

<footer>
    <div class="container">

        <div class="copy text-center">
            <a href='#'></a>
        </div>

    </div>
</footer>

<link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/table1.js"></script>
<!-- jQuery UI -->
<script src="js/table2.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="vendors/datatables/js/jquery.dataTables.min.js"></script>

<script src="vendors/datatables/dataTables.bootstrap.js"></script>

<script src="js/custom.js"></script>
<script src="js/tables.js"></script>
</body>
</html>