<!DOCTYPE html>
<html lang="en" xmlns:color="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <title>Government Portal</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="gov.jpg" rel="icon">
  <link href="gov" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="body">

  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="fa fa-envelope-o"></i> <a href="mailto:governmentportal@gov.in">GovernmentPortal@gov.in</a>
        <i class="fa fa-phone"></i> +91 9011185988
      </div>


      <div class="social-links float-right">
        <a href="https://twitter.com/login?lang=en" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.instagram.com/" class="instagram"><i class="fa fa-instagram"></i></a>
        <a href="https://plus.google.com/discover" class="google-plus"><i class="fa fa-google-plus"></i></a>
        <a href="https://www.linkedin.com/start/join" class="linkedin"><i class="fa fa-linkedin"></i></a>
      </div>
    </div>
  </section>


  <div id="google_translate_element"></div>

  <script type="text/javascript">
      function googleTranslateElementInit() {
          new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
      }
  </script>

  <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto">Reve<span>al</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#body">Home</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#services">Sectors</a></li>
          <li><a href="#portfolio">Portfolio</a></li>
          <li><a href="#contact">Contact</a></li>
          <li><a href="Sign_up_Form.php">Sign Up</a></li>
          <li class="menu-has-children"><a href="LoginForm.php">Login</a>
          </li>

        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">

    <div class="intro-content">
      <h2>Government Schemes Portal</h2>

          <div class="container">
              <br/>
              <div class="row justify-content-center">
                  <div class="col-12 col-md-10 col-lg-8">
                      <form class="card card-sm" action="tables/search.php" method="post">
                          <div class="card-body row no-gutters align-items-center">

                              <!--end of col-->
                              <div class="col">
                                  <input id="name" name="name" class="form-control form-control-lg form-control-borderless" type="search" placeholder="Search topics or keywords">
                              </div>
                              <!--end of col-->
                              <div class="col-auto">
                                 <button class="btn btn-lg" style="background: #50d8af;" type="submit"> Search</button>
                              </div>

                             <!--end of col-->
                          </div>
                      </form>
                      <br>
                      <button class="btn btn-lg" style="background: #50d8af; width : 30%; height: 30%" type="submit"><a href="EligibilityForm.php" style="color : black;"> Eligibility criteria</a></button>
                      <button class="btn btn-lg" style="background: #50d8af; width : 30%; height: 30%" type="submit"><a href="AddSchemeForm.php" style="color : black;"> Add Scheme</a></button>
                  </div>
                  <!--end of col-->
              </div>
          </div>
    </div>



    <div id="intro-carousel" class="owl-carousel" >
      <div class="item" style="background-image: url('img/intro-carousel/1.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/2.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/3.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/4.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/5.jpg');"></div>
    </div>

  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Section
    ============================-->
    <section id="about" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 about-img">
            <img src="gov.jpg" alt="">
          </div>

          <div class="col-lg-6 content">
            <h2>Government Schemes Portal</h2>
            <h3>Here you can find all the Schemes hosted by Government of India for Public Welfare</h3>

            <ul>
              <li><i class="ion-android-checkmark-circle"></i> Check your eligibility criteria for any scheme.</li>
              <li><i class="ion-android-checkmark-circle"></i> Search Schemes categorized in various sectors.</li>
              <li><i class="ion-android-checkmark-circle"></i> Get information of any new Scheme as soon as it is hosted. </li>
            </ul>

          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">
        <div class="section-header">
          <h2>Sectors</h2>
          <p>Schemes are categorized in following sectors</p>
        </div>

        <div class="row">

          <div class="col-lg-4">
            <div class="box wow fadeInLeft">
              <div class="icon"><i class="fa  fa-graduation-cap"></i></div>
              <h4 class="title"><a href="tables/Education.php">Education</a></h4>
              <p class="description">Various types of Scholarships for students nationwide<br></p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInRight">
              <div class="icon"><i class="fa fa-leaf"></i></div>
              <h4 class="title"><a href="tables/Agriculture.php">Agriculture</a></h4>
              <p class="description">Incentive on crops, Proper market price for crop</p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-ambulance"></i></div>
              <h4 class="title"><a href="tables/Health.php">Health</a></h4>
              <p class="description">Medical Checkup Camps, Vaccination Camps, Discounts on Medicines</p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box wow fadeInRight" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-gift"></i></div>
              <h4 class="title"><a href="tables/Pension.php">Pension & Benefits</a></h4>
              <p class="description">Pension plans for retired people</p>
            </div>
          </div>

           <div class="col-lg-4">
             <div class="box wow fadeInLeft">
                    <div class="icon"><i class="fa  fa-money"></i></div>
                    <h4 class="title"><a href="tables/Money.php">Money & Taxes</a></h4>
                    <p class="description">Taxation schemes, Investment schemes</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box wow fadeInRight" data-wow-delay="0.2s">
                    <div class="icon"><i class="fa  fa-plane"></i></div>
                    <h4 class="title"><a href="tables/Visa.php">Visas & Passports</a></h4>
                    <p class="description">Visa and Passport related schemes</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box wow fadeInLeft">
                    <div class="icon"><i class="fa  fa-lightbulb-o"></i></div>
                    <h4 class="title"><a href="tables/Electricity.php">Electricity</a></h4>
                    <p class="description">Schemes to provide electircity to rural areas</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box wow fadeInRight">
                    <div class="icon"><i class="fa  fa-briefcase"></i></div>
                    <h4 class="title"><a href="tables/Jobs.php">Jobs</a></h4>
                    <p class="description">Various job related schemes for freshers</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box wow fadeInLeft">
                    <div class="icon"><i class="fa fa-bus"></i></div>
                    <h4 class="title"><a href="tables/Transport.php">Transport</a></h4>
                    <p class="description">Schemes to promote public transport</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box wow fadeInRight">
                    <div class="icon"><i class="fa fa-cogs"></i></div>
                    <h4 class="title"><a href="tables/Manufacturing.php">Manufacturing</a></h4>
                    <p class="description">Schemes to promote manufacturing of goods in the country</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box wow fadeInLeft">
                    <div class="icon"><i class="fa fa-line-chart"></i></div>
                    <h4 class="title"><a href="tables/Business.php">Business</a></h4>
                    <p class="description">Schemes to promote businesses in nation<br><br></p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box wow fadeInRight">
                    <div class="icon"><i class="fa fa-trash"></i></div>
                    <h4 class="title"><a href="tables/Environment.php">Environment</a></h4>
                    <p class="description">Schemes to protect the environment and to prevent the pollution</p>
                </div>
            </div>


        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Partners</h2>
          <p>The Government ties with different Partners so as to reach out to more and more people to help them and fulfill their needs</p>
        </div>

        <div class="owl-carousel clients-carousel">
          <img src="img/clients/client-1.png" alt="">
          <img src="img/clients/client-2.png" alt="">
          <img src="img/clients/client-3.png" alt="">
          <img src="img/clients/client-4.png" alt="">
          <img src="img/clients/client-5.png" alt="">
          <img src="img/clients/client-6.png" alt="">
          <img src="img/clients/client-7.png" alt="">
          <img src="img/clients/client-8.png" alt="">
        </div>

      </div>
    </section><!-- #clients -->

    <!--==========================
      Our Portfolio Section
    ============================-->
    <section id="portfolio" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Portfolio</h2>
        </div>
      </div>

        <div class="row no-gutters" align="center">
            <div class="video-grid col-lg-3">
                <iframe src="https://www.youtube.com/embed/I-hIMIapD48" allowfullscreen></iframe>
            </div>
            <div class="video-grid col-lg-3">
                <iframe src="https://www.youtube.com/embed/IdTGaCMwfBI" allowfullscreen></iframe>
            </div>
            <div class="video-grid col-lg-3">
                <iframe src="https://www.youtube.com/embed/Ssu2Q3wS_Hw" allowfullscreen></iframe>
            </div>
            <div class="video-grid col-lg-3">
                <iframe src="https://www.youtube.com/embed/OTkWRB0Cwrc" allowfullscreen></iframe>
            </div>
        </div><br>


        <div class="row no-gutters">

          <div class="col-lg-3 col-md-4">
            <div class="portfolio-item wow fadeInUp">
              <a href="" class="portfolio-popup">
                <img src="agri2.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="portfolio-info"><h2 class="wow fadeInUp">Crop</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="portfolio-item wow fadeInUp">
              <a href="img/portfolio/2.jpg" class="portfolio-popup">
                <img src="agri1.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="portfolio-info"><h2 class="wow fadeInUp">Farm</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="portfolio-item wow fadeInUp">
              <a href="img/portfolio/3.jpg" class="portfolio-popup">
                <img src="edu1.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="portfolio-info"><h2 class="wow fadeInUp">Education</h2></div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="portfolio-item wow fadeInUp">
              <a href="img/portfolio/4.jpg" class="portfolio-popup">
                <img src="environment1.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="portfolio-info"><h2 class="wow fadeInUp">Environment</h2></div>
                </div>
              </a>
            </div>
          </div>

        </div>


    </section><!-- #portfolio -->


    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Contact Us</h2>
          <p>The objective behind the Portal is to provide a single window access to the information and services being provided by the Indian Government for citizens and other stakeholders. The content of this Portal is managed centrally by the National Portal Content Management Team from the National Portal Secretariat.</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address>Mumbai</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+919011185988">+91 9011185988</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:info@example.com">GovernmentPortal@gov.in</a></p>
            </div>
          </div>

        </div>
      </div>


        <div class="container">
          <div class="section-header"><h2>Mailing System</h2>
          </div>

          <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="mail.php" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="comment" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit">Send Mail</button></div>
          </form>
        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Government Portal<strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Reveal
        -->
        Designed by <a href="https://bootstrapmade.com/">Indian Government</a>
      </div>
    </div>

  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>
  <!-- Uncomment below if you want to use dynamic Google Maps -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script> -->

  <!-- Contact Form JavaScript File -->

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>


<div class='sk-ww-twitter-feed' data-embed-id='14328'></div><script src='https://www.sociablekit.com/app/embed/twitter-feed/widget.js'></script>


</body>
</html>
