<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<h2 style="text-align: center">Add Scheme</h2>
<form class="form-horizontal" method="post" action="scheme_added.php">
    <br>
    <br>
    <div class="form-group">

        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Scheme Name:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="name" placeholder="Enter scheme name"  name="name">
            </div>
        </div>
    </div>

    <div class="form-group">
    <div class="col-xs-12">
        <div class="col-xs-4"></div>
        <div class="col-xs-4">
            <label>Scheme Info:</label>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="col-xs-4"></div>
        <div class="col-xs-4">
            <textarea class="form-control" id="details" name="details" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Scheme Info"></textarea>
        </div>
    </div>
    </div>




    <div class="form-group">
    <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Age:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="number" class="form-control" id="elig_age" placeholder="Enter age" name="elig_age" min="0">
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>State:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="elig_state" placeholder="Enter state name" name="elig_state">
            </div>
        </div>
    </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Occupation:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="elig_occupation" placeholder="Enter occupation" name="elig_occupation">
            </div>
        </div>
    </div>
    </div>



    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Required Documents</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <textarea class="form-control" id="terms" name="terms" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Required Documents"></textarea>
            </div>
        </div>
    </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>Type:</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="type" placeholder="Enter type " name="type">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <label>URL</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="url" placeholder="Enter URL" name="url">
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-5">
                <button type="submit" class="btn btn-default" style="text-align: center">Submit</button>
            </div>
        </div>
    </div>
</form>


