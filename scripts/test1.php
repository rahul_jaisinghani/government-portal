
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="style1.css" />
    <link rel="stylesheet" type="text/css" href="style.css" />

    <link rel = "stylesheet"
          type = "text/css"
          href = "rr.css" />

    <meta charset="utf-8">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #050505;
            color: white;
        }
    </style>

    <title>Books Details</title>
</head>
<body>
<div id="header">
    <div id="logo">
        <img class="pic alignleft" src="vesit.png" width="120" height="120">
        <h1>CMPN Departmant Library, VESIT APPLICATION</h1>
    </div>
    <div id="slogan">
        <a href="downloadbooks.php" class="push_button blue">Download</a>
    </div>
</div>
<div id="menu">
    <ul>
        <li><a href="main.php"><div class="dropdown"><button class="dropbtn">HOME</button></div></a></li>
        <li><div class="dropdown">
                <button class="dropbtn">BOOK</button>
                <div class="dropdown-content">
                    <a href="btable.php">LIST</a>
                    <a href="add.php">ADD</a>
                    <a href="remove.php">REMOVE</a>
                    <a href="update.php">UPDATE</a>
                </div>
            </div></li>
        <li><div class="dropdown">
                <button class="dropbtn">STUDENT</button>
                <div class="dropdown-content">
                    <a href="student.php">STUDENT ADD</a>
                    <a href="search.php">STUDENT SEARCH</a>
                    <a href="stable.php">STUDENT TABLE</a>
                </div>
            </div>
        </li>
        <li><a href="etable.php"><div class="dropdown"><button class="dropbtn">EXPIRY_LIST</button></div></a></li>
        <li><a href="mail.php"><div class="dropdown"><button class="dropbtn">MAIL</button></div></a></li>
        <li><a href="help.php"><div class="dropdown"><button class="dropbtn">HELP</button></div></a></li>

        <li class="last"><div class="dropdown"><a href="contact.php"><button class="dropbtn">CONTACT</button></div></a></li>
        <li><a href="logout.php"><div class="dropdown"><button class="dropbtn">LOGOUT</button></div></a></li>
    </ul>
</div>
<br>

<?php



    require 'connection.php';
    $conn    = Connect();
    $query   = "SELECT * FROM scheme";
    $success = $conn->query($query) or die(mysql_error());
    ;

    if (!$success) {
        die("Couldn't enter data: ".$conn->error);

    }





?>
<table border="2" style= "margin: 0 auto; width=100%" >
    <thead>
    <tr>
        <th>Book Name</th>
        <th>Book Code</th>
        <th>ISBN No.</th>
        <th>Author</th>
        <th>Section</th>
        <th>Publication</th>
        <th>Quantity</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;



    while ($row = mysqli_fetch_array($success)) {

        $class = ($i == 0) ? "" : "alt";
        echo "<tr class=\"".$class."\">";
        echo "<td>".$row['id']."</td>";
        echo "<td>".$row['name']."</td>";
        echo "<td>".$row['elig_age']."</td>";
        echo "<td>".$row['elig_state']."</td>";
        echo "<td>".$row['details']."</td>";
        echo "<td>".$row['terms']."</td>";
        echo "<td>".$row['type']."</td>";
        echo "<td>".$row['name']."</td>";
        echo "</tr>";
        $i = ($i==0) ? 1:0;
    }

    ?>
    </tbody>
</table>

</body>
</html>