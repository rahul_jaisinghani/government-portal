function ValidateContactForm()
{
    var bcode = document.ContactForm.BCode;

	if (bcode.value == "")
    {
        window.alert("Please enter the book code.");
        bcode.focus();
        return false;
    }
	if (bcode.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode.focus();
        return false;
    }
    return true;
}