# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 15:46:13 2018

@author: rahul jaisinghani
"""

from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
  return render_template('template.html')

@app.route('/my-link/')
def my_link():
  print('I got clicked!')

  return 'Click.'

if __name__ == '__main__':
  app.run(debug=True)