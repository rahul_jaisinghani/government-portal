<?php
 session_start();


if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}

 else {
    echo "Please log in first to see this page.";
	header("Location: login1.php");
	
}
?>

	<html>
	<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel = "stylesheet"
   type = "text/css"
   href = "rr.css" />
   
	<meta charset="utf-8">  
	<script src="add.js">
	</script> 
	</head>
	<body onload="document.ContactForm.BName.focus();">
	<div id="header">
		<div id="logo">
		<img class="pic alignleft" src="vesit.png" width="120" height="120">
		<h1>CMPN Departmant Library, VESIT APPLICATION</h1>
		</div>
		<div id="slogan">
			<h2></h2>
		</div>
	</div>
		<div id="menu">
		<ul>
			<li><a href="main.php"><div class="dropdown"><button class="dropbtn">HOME</button></div></a></li>
			<li><div class="dropdown">
  <button class="dropbtn">BOOK</button>
  <div class="dropdown-content">
  <a href="btable.php">LIST</a>
    <a href="add.php">ADD</a>
    <a href="remove.php">REMOVE</a>
    <a href="update.php">UPDATE</a>
  </div>
</div></li>
			<li><div class="dropdown">
  <button class="dropbtn">STUDENT</button>
  <div class="dropdown-content">
    <a href="student.php">STUDENT ADD</a>
    <a href="search.php">STUDENT SEARCH</a>
    <a href="stable.php">STUDENT TABLE</a>
  </div>
</div>
</li>
			<li><a href="etable.php"><div class="dropdown"><button class="dropbtn">EXPIRY_LIST</button></div></a></li>
			<li><a href="mail.php"><div class="dropdown"><button class="dropbtn">MAIL</button></div></a></li>
			<li><a href="#"><div class="dropdown"><button class="dropbtn">HELP</button></div></a></li>
			<li class="last"><div class="dropdown"><a href="contact.php"><button class="dropbtn">CONTACT</button></div></a></li>
			<li><a href="logout.php"><div class="dropdown"><button class="dropbtn">LOGOUT</button></div></a></li>	
		</ul>
	</div>
<p>
<b><font size="5">Our website is basically based on the library management system.
It has following functionality:</font></b></p>
<font size="4">
<p>1]<b>Add:</b>Add is used to add new books(which does not exist previously in library).New book should have a unique Book code.</p>

<p>2]<b>Remove:</b>Remove is used to remove the books from the library.If the book dose not exist it will show a error.</p>

<p>3]<b>Update:</b>Update is used to update existing book.It takes book code as a parameter and search for the book.If the book does not exist it will show a error.</p>

<p>4]<b>List:</b>List consists on the list of books existing in the library.It shows how muvh quantity of books is present in the library.</p>

<p>5]<b>Student Add:</b>If a student takes the books from the library then this functionality is used to add  students name in the list of the library.</p>

<p>6]<b>Student Search:</b>It is used to search the name of a particular student existing in the list for updation of transactions.</p>

<p>7]<b>Student Table:</b>It consists of the names of students who have taken books from the library.</p>

<p>8]<b>Update button:</b>It is used to update the student details in the student table.</p>

<p>9]<b>Submitted button:</b>It is used to remove the name of the student who have returned the books from the student list of library.</p>

<p>10]<b>Help:</b>It is used as a user manual consisting of details and functionality of the website.</p>

<p>11]<b>Contact Us:</b> It consists of the names of the developers of the website and their contact details.</p>

<p>12]<b>Expiry list:</b>It consists of the names of the students who have taken the books but have not returned the books on date.</p>

<p>13]<b>Mail:</b>It is used to mail the students to return the books if they have not returned on time.It's a reminder to the students.In this the user needs to enter the email id of the student and submit it.</p>

<p>14]<b>Logout:</b>It is used to logout from the website</p>
</font>
</body>
</html>