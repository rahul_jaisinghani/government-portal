<?php




?>
<?php
if(isset($_POST['SubmitButton'])){ //check if form was submitted
    #$input = $_POST['inputText']; //get input text
    $message = "Success! You entered: ";
    require 'connection.php';
    $conn    = Connect();
    date_default_timezone_set('Asia/Calcutta');

    $date1 = date("Y-m-d H:i:s");

    $FName    = $conn->real_escape_string($_POST['FName']);
    $LName    = $conn->real_escape_string($_POST['LName']);
    $Class = $conn->real_escape_string($_POST['Class']);
    $Roll = $conn->real_escape_string($_POST['Roll']);
    $Email = $conn->real_escape_string($_POST['Email']);
    $Phone = $conn->real_escape_string($_POST['Phone']);
    $BCode1 = $conn->real_escape_string($_POST['BCode1']);
    $BCode2 = $conn->real_escape_string($_POST['BCode2']);

    echo $Email;
    $query1   = "INSERT INTO `request` ( `FName`, `LName`, `Class`, `Roll`, `Email`, `Phone`, `BCode1`, `BCode2`,`Accept`, `date`) VALUES ('$FName', '$LName', '$Class', '$Roll', '$Email', '$Phone', '$BCode1', '$BCode2', 'False', '$date1')";
    $success = $conn->query($query1);

    if (!$success) {
        die("Couldn't enter data: ".$conn->error);

    }

    require("PHPMailer_5.2.4/class.phpmailer.php");
    $mail = new PHPMailer();
    $mail->IsSMTP(); // set mailer to use SMTP
    $mail->SMTPDebug  = 3;
    $mail->From = "rahul.jaisinghani@ves.ac.in";
    $mail->FromName = "Rahul";
    $mail->Host = "smtp.gmail.com"; // specif smtp server
    $mail->SMTPSecure= "ssl"; // Used instead of TLS(587) since port is 465
    $mail->Port = 465; // Used instead of 587 when only POP mail is selected
    $mail->SMTPAuth = true;
    $mail->Username = "2015rahul.jaisinghani@ves.ac.in"; // SMTP username
    $mail->Password = "ilovemac"; // SMTP password
    $mail->AddAddress($Email, "From"); //replace myname and mypassword to yours
    $mail->AddReplyTo("2015rahul.jaisinghani@ves.ac.in", "Rahul");
    $mail->WordWrap = 50; // set word wrap

//$mail->AddAttachment(""); // add attachments
//$mail->AddAttachment("");

    $mail->IsHTML(true); // set email format to HTML
    $mail->Subject = 'Request For Book is submitted Successfully';
    $mail->Body = 'Your Request For Book is submitted Successfully, Please Wait for acceptance';

    if($mail->Send()) {echo "Send mail successfully";header("Location: reqsubmitted.php");}
    else {echo "Send mail fail";}

    $conn->close();

}
else {
    ?>


    <html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <link rel="stylesheet" type="text/css" href="rr.css"/>
        <meta charset="utf-8">
        <script src="student.js">

        </script>
    </head>
    <body onload="document.ContactForm.FName.focus();">
    <div id="header">
        <div id="logo">
            <img class="pic alignleft" src="vesit.png" width="120" height="120">
            <h1>CMPN Departmant Library, VESIT APPLICATION</h1>
        </div>
        <div id="slogan">
            <h2></h2>
        </div>
    </div>
    <div id="menu">

    </div>
    <form method="post" name="ContactForm" onSubmit="return ValidateContactForm()">
        <div id="header1"><h1>Students details :</h1></div>

        <div class="contentform">
            <div class="leftcontact">
                <div class="form-group">
                    <p>First Name<span>*</span></p>
                    <span class="icon-case"></span>
                    <input type="text" name="FName">
                </div>
                <div class="form-group">
                    <p>Last Name<span>*</span></p>
                    <span class="icon-case"></span>
                    <input type="text" name="LName">
                </div>
                <div class="form-group">
                    <p>Class<span>*</span></p>
                    <span class="icon-case"></span>
                    <input type="text" name="Class">
                </div>
                <div class="form-group">
                    <p>Roll No.<span>*</span></p>
                    <span class="icon-case"></span>
                    <input type="number" name="Roll">
                </div>


            </div>
            <div class="rightcontact">


                <div class="form-group">
                    <p>Email ID<span>*</span></p>
                    <span class="icon-case"></span>
                    <input type="email" name="Email">
                </div>

                <div class="form-group">
                    <p>Phone No.<span>*</span></p>
                    <span class="icon-case"></span>
                    <input type="number" name="Phone">

                    <div class="form-group">
                        <p>Book Code of Book1<span>*</span></p>
                        <span class="icon-case"></span>
                        <input type="text" name="BCode1" value="C-000"/>
                    </div>
                    <div class="form-group">
                        <p>Book Code of Book2<span>*</span></p>
                        <span class="icon-case"></span>
                        <input type="text" name="BCode2" value="C-000"/>
                    </div>
                </div>


            </div>
        </div>
        <button type="submit" class="bouton-contact" name="SubmitButton">Submit</button>

    </form>

    </body>
    </html>
    <?php
}?>