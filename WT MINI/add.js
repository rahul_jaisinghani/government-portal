function ValidateContactForm()
{
    var bname = document.ContactForm.BName;
    var bcode = document.ContactForm.BCode;
    var isbn = document.ContactForm.Isbn;
    var author = document.ContactForm.Author;
    var quantity = document.ContactForm.Quantity;
    var date = document.ContactForm.Date;

	if (bname.value == "")
    {
        window.alert("Please enter the book name.");
        bname.focus();
        return false;
    }
	if (bcode.value == "")
    {
        window.alert("Please enter the book code.");
        bcode.focus();
        return false;
    }
	if (bcode.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode.focus();
        return false;
    }

	if (isbn.value == "")
    {
        window.alert("Please enter the ISBN No.");
        isbn.focus();
        return false;
    }
	if (author.value == "")
    {
        window.alert("Please enter the Author name.");
        author.focus();
        return false;
    }
	if (quantity.value == "")
    {
        window.alert("Please enter the Quantity.");
        quantity.focus();
        return false;
    }
	if (date.value == "")
    {
        window.alert("Please enter the Date.");
        date.focus();
        return false;
    }
    return true;
}