# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 13:35:32 2018

@author: rahul jaisinghani
"""
def send_email(user, pwd, recipient, subject, body):
    import smtplib

    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(user, pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print('successfully sent the mail')
    except:
        print("failed to send mail")
        
        
send_email('2015rahul.jaisinghani@ves.ac.in','QWERTYUIOP','rahuljaisinghani1997@gmail.com','jj','Return My books')