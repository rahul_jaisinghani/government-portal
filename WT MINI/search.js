function ValidateContactForm()
{
    var fname = document.ContactForm.FName;
    var lname = document.ContactForm.LName;
 
	if (fname.value == "")
    {
        window.alert("Please enter the first name.");
        fname.focus();
        return false;
    }
	if (lname.value == "")
    {
        window.alert("Please enter the last name.");
        lname.focus();
        return false;
    }
  return true;
}