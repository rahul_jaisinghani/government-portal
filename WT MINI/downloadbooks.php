<?php  
  
require 'connection.php';
$conn    = Connect();  
  
$query = "SELECT * from add_book where BCode != 'C-000'";
 
  $success = $conn->query($query);
$columnHeader = '';  
$columnHeader = "Book Name" . "\t" . "Code" . "\t" . "Isbn" . "\t" . "Author" .  "\t" . "Section" . "\t  " . "Publication" .  "\t" . "Quantity" .  "\t" .  "Date" ;
  
$setData = '';  
  
while ($rec = mysqli_fetch_row($success)) {  
    $rowData = '';  
    foreach ($rec as $value) {  
        $value = '"' . $value . '"' . "\t";  
        $rowData .= $value;  
    }  
    $setData .= trim($rowData) . "\n";  
}  
  
  
header("Content-type: application/vnd.ms-excel");  
header("Content-Disposition: attachment; filename=Books.xls");  
header("Pragma: no-cache");  
header("Expires: 0");  
  
echo ucwords($columnHeader) . "\n" . $setData . "\n";  
  
?>