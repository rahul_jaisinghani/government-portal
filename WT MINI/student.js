function ValidateContactForm()
{
    var fname = document.ContactForm.FName;
    var lname = document.ContactForm.LName;
	var clas = document.ContactForm.Class;
	var roll = document.ContactForm.Roll;
    var email = document.ContactForm.Email;
    var phone = document.ContactForm.Phone;
    var bcode1 = document.ContactForm.BCode1;
    var bcode2 = document.ContactForm.BCode2;
	var bcode3 = document.ContactForm.BCode3;
	var bcode4 = document.ContactForm.BCode4;
	var bcode5 = document.ContactForm.BCode5;
	var bcode6 = document.ContactForm.BCode6;
	var dissu = document.ContactForm.Dissue;
    var dretur = document.ContactForm.Dreturn;


    if (fname.value == "")
    {
        window.alert("Please enter the first name.");
        fname.focus();
        return false;
    }
    if (lname.value == "")
    {
        window.alert("Please enter the last name.");
        lname.focus();
        return false;
    }
	
    if (clas.value == "")
    {
        window.alert("Please enter the class.");
        clas.focus();
        return false;
    }
	if (roll.value == "")
    {
        window.alert("Please enter the roll no.");
        roll.focus();
        return false;
    }
    if (email.value == "")
    {
        window.alert("Please enter a valid e-mail address.");
        email.focus();
        return false;
    }
	    if (phone.value == "")
    {
        window.alert("Please enter the phone number.");
        phone.focus();
        return false;
    }
    if (email.value.indexOf("@", 0) < 0)
    {
        window.alert("Please enter a valid e-mail address.");
        email.focus();
        return false;
    }
    if (email.value.indexOf(".", 0) < 0)
    {
        window.alert("Please enter a valid e-mail address.");
        email.focus();
        return false;
    }
	if (bcode1.value == "")
    {
        window.alert("Please enter the book code.");
        bcode1.focus();
        return false;
    }
	if (bcode1.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode1.focus();
        return false;
    }
	if (bcode2.value == "")
    {
        window.alert("Please enter the book code.");
        bcode2.focus();
        return false;
    }
	if (bcode2.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode2.focus();
        return false;
    }
	if (bcode3.value == "")
    {
        window.alert("Please enter the book code.");
        bcode3.focus();
        return false;
    }
	if (bcode3.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode3.focus();
        return false;
    }
	if (bcode4.value == "")
    {
        window.alert("Please enter the book code.");
        bcode4.focus();
        return false;
    }
	if (bcode4.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode4.focus();
        return false;
    }
	if (bcode5.value == "")
    {
        window.alert("Please enter the book code.");
        bcode5.focus();
        return false;
    }
	if (bcode5.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode5.focus();
        return false;
    }
	if (bcode6.value == "")
    {
        window.alert("Please enter the book code.");
        bcode6.focus();
        return false;
    }
	if (bcode6.value.indexOf("C")!=0)
    {
        window.alert("Please enter a valid Book Code starting with C.");
        bcode6.focus();
        return false;
    }
	if (dissu.value == "")
    {
        window.alert("Please enter the Date of Issue.");
        dissu.focus();
        return false;
    }
	if (dretur.value == "")
    {
        window.alert("Please enter the Date of Return.");
        dretur.focus();
        return false;
    }
	if ((new Date(dissu.value).getTime() > new Date(dretur.value).getTime()))
    {
        window.alert("Please enter the valid Date of Return.");
        dretur.focus();
        return false;
    }
    return true;
}
