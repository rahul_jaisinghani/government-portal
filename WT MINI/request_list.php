<?php
session_start();
?>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel = "stylesheet"
          type = "text/css"
          href = "rr.css" />
    <meta charset="UTF-8">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #050505;
            color: white;
        }
        .button {
            background-color: #209D9D; /* Green */
            border: none;
            color: #ffffff;
            padding: 7px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 2px;
            cursor: pointer;

        }
    </style>
    <title>Student Details</title>
</head>
<body>
<div id="header">
    <div id="logo">
        <img class="pic alignleft" src="vesit.png" width="120" height="120">
        <h1>CMPN Departmant Library, VESIT APPLICATION</h1>
    </div>
    <div id="slogan">
        <h2></h2>
    </div>
</div>
<div id="menu">
    <ul>
        <li><a href="main.php"><div class="dropdown"><button class="dropbtn">HOME</button></div></a></li>
        <li><div class="dropdown">
                <button class="dropbtn">BOOK</button>
                <div class="dropdown-content">
                    <a href="btable.php">LIST</a>
                    <a href="add.php">ADD</a>
                    <a href="remove.php">REMOVE</a>
                    <a href="update.php">UPDATE</a>
                </div>
            </div></li>
        <li><div class="dropdown">
                <button class="dropbtn">STUDENT</button>
                <div class="dropdown-content">
                    <a href="student.php">STUDENT ADD</a>
                    <a href="search.php">STUDENT SEARCH</a>
                    <a href="stable.php">STUDENT TABLE</a>
                </div>
            </div>
        </li>
        <li><a href="etable.php"><div class="dropdown"><button class="dropbtn">EXPIRY_LIST</button></div></a></li>
        <li><a href="mail.php"><div class="dropdown"><button class="dropbtn">MAIL</button></div></a></li>
        <li><a href="help.php"><div class="dropdown"><button class="dropbtn">HELP</button></div></a></li>
        <li class="last"><div class="dropdown"><a href="contact.php"><button class="dropbtn">CONTACT</button></div></a></li>
        <li><a href="logout.php"><div class="dropdown"><button class="dropbtn">LOGOUT</button></div></a></li>
    </ul>
</div>
<br>
<?php


if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

    require 'connection.php';
    $conn    = Connect();
    $query   = "SELECT * FROM request";
    $success = $conn->query($query) or die(mysql_error());
    ;

    if (!$success) {
        die("Couldn't enter data: ".$conn->error);

    }
} else {
    echo "Please log in first to see this page.";
    header("Location: login1.php");

}




?>
<div style="overflow: scroll">
    <table border="2" style= "margin: 0 auto; width=100%" >
        <thead>
        <tr>
            <th>Date</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Class</th>
            <th>Roll no.</th>
            <th>Email ID</th>
            <th>Phone No.</th>
            <th>BCode1</th>
            <th>BCode2</th>
            <th>Accept</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;



        while ($row = mysqli_fetch_array($success)) {

            $class = ($i == 0) ? "" : "alt";
            echo "<tr class=\"".$class."\">";
            echo "<td>".$row['date']."</td>";
            echo "<td>".$row['FName']."</td>";
            echo "<td>".$row['LName']."</td>";
            echo "<td>".$row['Class']."</td>";
            echo "<td>".$row['Roll']."</td>";
            echo "<td>".$row['Email']."</td>";
            echo "<td>".$row['Phone']."</td>";
            echo "<td>".$row['BCode1']."</td>";
            echo "<td>".$row['BCode2']."</td>";
            if($row['Accept'] == 'False') {
                echo "<td><button class='button' type=button><a href='reqaccept.php?ID=" . $row['ID'] . "&Email=" . $row['Email'] . "'><font color='white'>Accept</font></a></button></td>";
            }
            else
            {
                echo "<td><button class='button' type=button disabled><font color='white'>Already Accepted</font></a></button></td>";

            }
            echo "</tr>";
            $i = ($i==0) ? 1:0;
        }

        ?>
        </tbody>
    </table></div>
</body>
</html>