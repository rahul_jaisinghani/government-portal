function ValidateContactForm()
{
    var ename = document.ContactForm.EName;
    var cname = document.ContactForm.CName;
	var date = document.ContactForm.Date;
	var salary = document.ContactForm.Salary;
    var email = document.ContactForm.Email;
    var phone = document.ContactForm.Phone;

    if (ename.value == "")
    {
        window.alert("Please enter  employee name.");
        ename.focus();
        return false;
    }
    if (cname.value == "")
    {
        window.alert("Please enter company name.");
        cname.focus();
        return false;
    }
	
    if (date.value == "")
    {
        window.alert("Please enter date of birth.");
        date.focus();
        return false;
    }
	if (salary.value == "")
    {
        window.alert("Please enter salary.");
        salary.focus();
        return false;
    }
    if (email.value == "")
    {
        window.alert("Please enter a valid e-mail address.");
        email.focus();
        return false;
    }
	    if (phone.value == "")
    {
        window.alert("Please enter your phone number.");
        phone.focus();
        return false;
    }
    if (email.value.indexOf("@", 0) < 0)
    {
        window.alert("Please enter a valid e-mail address.");
        email.focus();
        return false;
    }
    if (email.value.indexOf(".", 0) < 0)
    {
        window.alert("Please enter a valid e-mail address.");
        email.focus();
        return false;
    }
    return true;
}